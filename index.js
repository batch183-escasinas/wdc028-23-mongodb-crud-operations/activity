db.rooms.insertOne({
	"name":"single",
	"accomodates":2,
	"price":1000,
	"description":"A simple room with all the basic necessities",
	"roomAvailable":10,
	"isAvailable":false
});
db.rooms.insertMany([
	{
		"name":"double",
		"accomodates":3,
		"price":2000,
		"description":"A room fit a small family going on a vacation",
		"roomAvailable":5,
		"isAvailable":false
	},{
		"name":"queen",
		"accomodates":4,
		"price":400,
		"description":"A room with a queen sized bed perfect for a simple getaway",
		"roomAvailable":15,
		"isAvailable":false
	}
]);
db.rooms.find({
		"name":"double"
});

db.rooms.updateOne(
	{"name":"queen"},
			{$set:{"roomAvailable":0}
			}
	
);
db.rooms.deleteMany(
	{"roomAvailable":0}
	
);